<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model {

    protected $fillable = ['name', 'creator_id', 'type_id', 'description'];
    protected $with = ['dates'];

    public function type() {
        return $this->belongsTo('App\EventType', 'type_id');
    }

    public function dates() {
      return $this->hasMany('App\EventDate');
    }

    public function toArray() {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'creator_id' => $this->creator_id,
            'type' => $this->type,
            'dates' => $this->dates()->get(),
        ];
    }
}
