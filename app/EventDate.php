<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventDate extends Model {

    protected $fillable = ['event_id', 'start', 'end', 'repetition'];

    public function toArray() {
        return [
            'id' => $this->id,
            'event_id' => $this->event_id,
            'start' => $this->start,
            'end' => $this->end,
            'repetition' => $this->repetition
        ];
    }
}
