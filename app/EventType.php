<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventType extends Model {

    protected $fillable = ['label'];

    public function toArray() {
        return [
            'id' => $this->id,
            'label' => $this->label,
        ];
    }
}
