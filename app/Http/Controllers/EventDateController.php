<?php
  namespace App\Http\Controllers;

  use Illuminate\Http\Request;
  use App\EventDate;

  class EventDateController extends Controller {

      public function index() {
          return EventDate::all();
      }

      public function show(EventDate $eventDate) {
          return $eventDate;
      }

      public function store(Request $request) {
          $eventDate = Event::create($request->all());
          return response()->json($eventDate, 201);
      }

      public function update(Request $request, EventDate $eventDate) {
          $eventDate->update($request->all());
          return response()->json($eventDate, 200);
      }

      public function delete(Request $request, EventDate $eventDate) {
          $eventDate->delete();
          return response()->json(null, 204);
      }
  }
?>
