<?php

use Faker\Generator as Faker;

$factory->define(App\Event::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence,
        'creator_id' => $faker->randomDigitNotNull,
        'type_id' => $faker->randomDigit,
    ];
});
