<?php

use Illuminate\Database\Seeder;

class EventsTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
      App\Event::truncate();
      $faker = \Faker\Factory::create();
      for ($i = 0; $i < 50; $i++) {
          App\Event::create([
              'name' => $faker->sentence,
              'creator_id' => $faker->randomDigitNotNull,
              'type_id' => $faker->numberBetween(1,10),
          ]);
      }
    }
}
