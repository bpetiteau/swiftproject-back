<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
      App\User::truncate();

      $faker = \Faker\Factory::create();

      $password = Hash::make('toptal');

      App\User::create([
          'name' => 'admin',
          'email' => 'admin@test.com',
          'password' => Hash::make('59432hiwux'),
          'username' => 'admin',
      ]);
      for ($i = 0; $i < 10; $i++) {
          App\User::create([
              'name' => $faker->name,
              'email' => $faker->email,
              'password' => $password,
              'username' => $faker->userName,
          ]);
      }
    }
}
