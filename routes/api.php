<?php

use Illuminate\Http\Request;
Use App\Event;
Use App\User;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// AUTHENTICATION ROUTES
// Route::post('register', 'Auth\RegisterController@register');
// Route::post('login', 'Auth\LoginController@login');
// Route::post('logout', 'Auth\LoginController@logout');
// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// API ROUTES
Route::group(['middleware' => 'auth:api'], function() {
    Route::get('events', 'EventController@index');
    Route::get('events/{event}', 'EventController@show');
    Route::post('events', 'EventController@store');
    Route::put('events/{event}', 'EventController@update');
    Route::delete('events/{event}', 'EventController@delete');
    Route::get('users/{user}/events', 'EventController@indexByUser');

    Route::get('users/{user}', 'UserController@show');
    Route::put('users/{user}', 'UserController@update');
    Route::delete('users/{user}', 'UserController@delete');

    Route::get('dates', 'EventDateController@index');
    Route::get('dates/{eventDate}', 'EventDateController@show');
    Route::put('dates/{eventDate}', 'EventDateController@update');
    Route::post('dates', 'EventDateController@store');
    Route::delete('dates/{eventDate}', 'EventDateController@delete');
});

Route::get('users', 'UserController@index');

Route::post('users', 'UserController@store');
